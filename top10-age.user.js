// ==UserScript==
// @name           RED: Include Upload Age in Top 10
// @description    In the Top 10, include how many hours ago a torrent was uploaded
// @author         _mclovin
// @version        0.2
// @match          https://redacted.ch/top10.php?type=torrents
// @match          https://redacted.ch/top10.php
// @run-at         document-end
// @namespace      _
// ==/UserScript==

(() => {
  "use strict";

  const HOURS_AGO = 10;
  const site_date = document.getElementById("site_date").innerText;
  const site_time = document.getElementById("site_time").innerText;
  const site_ts = new Date(`${site_date} ${site_time}`).getTime();

  const throttle = ms => new Promise(resolve => setTimeout(resolve, ms));
  const elapsed = age_ms => {
    let hours = Math.floor((age_ms / (1000 * 60 * 60)) % 72);
    let minutes = Math.floor((age_ms / (1000 * 60)) % 60);
    hours = hours < 10 ? "0" + hours : hours;
    minutes = minutes < 10 ? "0" + minutes : minutes;
    return `${hours}:${minutes}`;
  };

  const hitAPI = async (torrentid, index) => {
    try {
      if (index > 4) await throttle(10000); // crudely avoid hitting the API more than 5 times per 10s
      const res = await fetch(`${location.origin}/ajax.php?action=torrent&id=${torrentid}`);
      return res.json();
    } catch (error) {
      console.log(error);
    }
  };

  const col = document.getElementsByClassName("colhead")[0];
  col.insertAdjacentHTML(
    "beforeend",
    `<td style="text-align: right;">Age(hrs)</td>`
  );
  const rows = [...document.getElementsByClassName("torrent")].slice(0, 10);

  rows.forEach((row, index) => {
    let torrentid = row.innerHTML.match(/torrentid=([0-9]+)/)[1];
    hitAPI(torrentid, index).then(({ response, status }) => {
      if (status !== "success") {
        console.log("API request failed; aborting.");
        return;
      }
      let upload_time = new Date(response.torrent.time).getTime();
      let age_ms = site_ts - upload_time;
      let style = age_ms < (HOURS_AGO * 3600000) ? 'style="color:red;"' : "";
      row.insertAdjacentHTML(
        "beforeend",
        `<td class="number_column" ${style}>${elapsed(age_ms)}</td>`
      );
    });
  });
})();
